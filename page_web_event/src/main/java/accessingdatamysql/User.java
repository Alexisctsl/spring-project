package accessingdatamysql;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity // This tells Hibernate to make a table out of this class
@Table(name = "db_user")
public class User{
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name = "id", unique = true, nullable = false)
  private Integer id;

  private String name;
  private String email;
  
  @ManyToOne(fetch = FetchType.LAZY, targetEntity = Evenement.class)
  private Evenement eventParticipated; 

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
  public Evenement getEvenement() {
		return eventParticipated;
	}

  public void setEvenement(Evenement evenement) {
	  this.eventParticipated = evenement;
	}
}