package accessingdatamysql;

import org.springframework.data.repository.CrudRepository;

import accessingdatamysql.Evenement;

public interface EventRepository extends CrudRepository<Evenement, Integer>  {

}
