# TP n°4 – Spring Boot + Hibernate

## Pré-requis 

- Pour faire fonctionner le projet il faut au préalable avoir java d'installer en local, si ce n'est pas fait installer le paquest openjdk11 : sudo apt install openjdk8-jdk

- Il faut également installer le pacquet postgres : sudo apt-get install postgres 

- Lancer psql.

- il faut ensuite créer un utilisateur dans postgres au nom d'admin : CREATE USER admin ;

- Ensuite, lui donner les droits de créer une base de données : ALTER ROLE admin WITH CREATEDB ;

- Ensuite, il est possible de créer une base de données, elle s'appelera participant : CREATE DATABASE particiapnt OWNER admin ;

- Il faut également ajouter un mot de passe à votre utilisateur : ALTER USER admin WITH ENCRYPTED PASSWORD 'admin' ;

- Votre base de données est prêtre à être utilisée. 

## Utilisation de l'application 

- Il faut dans un premier temps éxécuter le code dans votre IDE, ensuite allez sur votre navigateur à l'adresse suivante : localhost:8081

- Il est possible de créer un évenement en cliquant sur le bouton "ajouter un évenement". Ensuite entrer un nom d'évement et appuyer su le bouton valider. 

- Pour créer un participant il faut retourner sur la page d'accueil et cliquer sur le bouton "ajouter un participant". Ensuite entrer un nom de participant et une addresse mail. Il faut ensuite cliquer sur la liste déroulant pour choisir un évenement déjà créé. Une fois que toutes les informations sont remplies, appuyer sur valider.

- Retouner sur la page d'accueil. En cliquant sur le bouton "Liste des partipants et leurs évenements", vous pourrez voir quels sont les participants incris et quelsont leurs évements. 

